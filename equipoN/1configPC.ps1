# Este script configura las maquinas virtuales, desactiva firewalls, les asigna direccian IP, mascara de red y DNS, les asigna nombre a las PC para finalmente reinicar cada una de las maquinas.

<#
NOTA IMPORTANTE!!!!
Dentro de cada maquina virtual, en nombre del adaptador de red es diferente, por lo que, para que el script funcione correctamente, ademas de tener las maquinas encendidas, se debe de verificar el nombre del adaptador de red para cada una.
Una vez obtenido el nombre, reemplazarlo si es necesario en la variable $interfaz de cada maquina.
El nombre del adaptador de red se puede ver usando el comando ipconfig
#>

# Nombre de las maquinas virtuales de cada equipo en donde se corre el script
$machineNameThundercats = "THUNDERCATSPC"
$machineNameTercerPlaneta = "TERCERPLANETAPC"
$machineNameThundera = "THUNDERAPC"
$machinenNameDHCP1 = "DHCPCore"
$machinenNameDHCP2 = "DHCPDE"

# Thundercats
Invoke-Command  -VMName $machineNameThundercats -ScriptBlock {
    # Variables
    $ipaddressThundercats = "10.0.20.1"
    $dnsaddressDomains = "127.0.0.1"
    $machineNameThundercats = "THUNDERCATSPC"
    $interfaz = "Ethernet 2"

    function config {
        Param($ipaddress, $dnsaddress, $machineName)
        # Desactivar Firewall
        Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
        netsh advfirewall Set allprofiles State Off 

        # Creamos una excepción al protocolo ICMP para probar conectividad
        netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

        #Configuración de IP y DNS 
        Disable-NetAdapterBinding -Name $interfaz -ComponentID ms_tcpip6 -PassThru
        New-NetIPAddress -InterfaceAlias $interfaz -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
        Set-DnsClientServerAddress -InterfaceAlias $interfaz -ServerAddresses $dnsaddress

        #Configuración de nombre de pc
        Rename-Computer -NewName $machineName -Restart
    }
    "Thundercats-configurando PC"
    config $ipaddressThundercats $dnsaddressDomains $machineNameThundercats
    "Termino configuracion de Thundercats"
}

# TercerPlaneta
Invoke-Command  -VMName $machineNameTercerPlaneta -ScriptBlock {
    # Variables
    $dnsaddressDomains = "127.0.0.1"
    $ipaddressTercerPlaneta = "10.0.21.1"
    $machineNameTercerPlaneta = "TERCERPLANETAPC"
    $interfaz = "Ethernet 2"

    function config {
        Param($ipaddress, $dnsaddress, $machineName)
        # Desactivar Firewall
        Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
        netsh advfirewall Set allprofiles State Off 

        # Creamos una excepción al protocolo ICMP para probar conectividad
        netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

        #Configuración de IP y DNS 
        Disable-NetAdapterBinding -Name $interfaz -ComponentID ms_tcpip6 -PassThru
        New-NetIPAddress -InterfaceAlias $interfaz -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
        Set-DnsClientServerAddress -InterfaceAlias $interfaz -ServerAddresses $dnsaddress

        #Configuración de nombre de pc
        Rename-Computer -NewName $machineName -Restart
    }
    "TercerPlaneta-configurando PC"
    config $ipaddressTercerPlaneta $dnsaddressDomains $machineNameTercerPlaneta
    "Termino configuracion de TercerPlaneta"
}

# Thundera
Invoke-Command  -VMName $machineNameThundera -ScriptBlock {
    # Variables
    $ipaddressThundercats = "10.0.20.1"
    $ipaddressThundera = "10.0.20.2"
    $machineNameThundera = "THUNDERAPC"
    $interfaz = "Ethernet"

    function config {
        Param($ipaddress, $dnsaddress, $machineName)
        # Desactivar Firewall
        Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
        netsh advfirewall Set allprofiles State Off 

        # Creamos una excepción al protocolo ICMP para probar conectividad
        netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

        #Configuración de IP y DNS 
        Disable-NetAdapterBinding -Name $interfaz -ComponentID ms_tcpip6 -PassThru
        New-NetIPAddress -InterfaceAlias $interfaz -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
        Set-DnsClientServerAddress -InterfaceAlias $interfaz -ServerAddresses $dnsaddress

        #Configuración de nombre de pc
        Rename-Computer -NewName $machineName -Restart
    }
    "Thundera-configurando PC"   
    config $ipaddressThundera $ipaddressThundercats $machineNameThundera
    "Termino configuracion de Thundera"
}

# DHCP1
Invoke-Command  -VMName $machinenNameDHCP1 -ScriptBlock {
    # Variables
    $ipaddressThundercats = "10.0.20.1"
    $ipaddressDC01 = "10.0.30.1"
    $machineNameDC01 = "DC01"
    $interfaz = "Ethernet"

    function config {
        Param($ipaddress, $dnsaddress, $machineName)
        powershell
        # Desactivar Firewall
        Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
        netsh advfirewall Set allprofiles State Off 

        # Creamos una excepción al protocolo ICMP para probar conectividad
        netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

        #Configuración de IP y DNS 
        Disable-NetAdapterBinding -Name $interfaz -ComponentID ms_tcpip6 -PassThru
        New-NetIPAddress -InterfaceAlias $interfaz -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
        Set-DnsClientServerAddress -InterfaceAlias $interfaz -ServerAddresses $dnsaddress

        #Configuración de nombre de pc
        Rename-Computer -NewName $machineName -Restart
    }
    "DHCP1-configurando PC"   
    config $ipaddressDC01 $ipaddressThundercats $machineNameDC01
    "Termino configuracion de DHCP1"
}

# DHCP2
Invoke-Command  -VMName $machinenNameDHCP2 -ScriptBlock {
    # Variables
    $ipaddressThundercats = "10.0.20.1"
    $ipaddressDC02 = "10.0.30.2"
    $machineNameDC02 = "DC02"
    $interfaz = "Ethernet 2"

    function config {
        Param($ipaddress, $dnsaddress, $machineName)
        # Desactivar Firewall
        Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
        netsh advfirewall Set allprofiles State Off 

        # Creamos una excepción al protocolo ICMP para probar conectividad
        netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

        #Configuración de IP y DNS 
        Disable-NetAdapterBinding -Name $interfaz -ComponentID ms_tcpip6 -PassThru
        New-NetIPAddress -InterfaceAlias $interfaz -IPAddress $ipaddress -AddressFamily IPv4 -PrefixLength 18
        Set-DnsClientServerAddress -InterfaceAlias $interfaz -ServerAddresses $dnsaddress

        #Configuración de nombre de pc
        Rename-Computer -NewName $machineName -Restart
    }
    "DHCP2-configurando PC"   
    config $ipaddressDC02 $ipaddressThundercats $machineNameDC02
    "Termino configuracion de DHCP2"
}