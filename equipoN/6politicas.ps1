﻿# Este script configura las politicas de contraseñas en thundercats.local para el grupo felinos

# Variables de nombres de maquinas virtuales
$machineNameThundercats = "THUNDERCATSPC"

Invoke-Command  -VMName $machineNameThundercats -ScriptBlock {
    #Creamos el grupo Felinos
    New-ADGroup -Name "Felinos" `
    -SamAccountName "Felinos" `
    -GroupCategory Security `
    -GroupScope Global `
    -DisplayName "Felinos" `
    -Path "DC=thundercats,DC=local" `
    -Description "Grupo Felinos" `
    -PassThru –Verbose

    #Crea un objeto que contiene las configuraciones generales de politica de contraseñas
    $TemplatePSO = New-Object Microsoft.ActiveDirectory.Management.ADFineGrainedPasswordPolicy
    $TemplatePSO.ComplexityEnabled = $false
    $TemplatePSO.LockoutDuration = [TimeSpan]::Parse("0.00:01:00")
    $TemplatePSO.LockoutObservationWindow = [TimeSpan]::Parse("0.00:01:00")
    $TemplatePSO.LockoutThreshold = 2
    $TemplatePSO.MinPasswordAge = [TimeSpan]::Parse("0.00:10:00")
    $TemplatePSO.PasswordHistoryCount = 24
    $TemplatePSO.ReversibleEncryptionEnabled = $false

    #Crea una instancia del objeto llamado Felinos 
    New-ADFineGrainedPasswordPolicy -Instance $TemplatePSO -Name "Felinos" -Precedence 1 -Description "Politica Felinos" -DisplayName "Felinos"  -MinPasswordLength 3

    #Añade la politica al grupo Felinos
    Add-ADFineGrainedPasswordPolicySubject "Felinos" -Subjects "Felinos"

    #Verifica la politica aplicada a Felinos
    #Get-ADFineGrainedPasswordPolicy "Felinos"

    #Verifica las politicas de contraseñas aplicadas a Felinos. Entre menor el numero mayor precedencia.
    #Get-ADGroup "Felinos" -Properties * | Select-Object msDS-PSOApplied
}