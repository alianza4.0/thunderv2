$path = "C:\Users\Usario\Desktop" #Se debe cambiar a donde se tiene el archivo portada.htm y se desea almacenar el ejecutable
#De preferencia mantener el archivo portada.htm en la misma ubicacion que el ejecutable fastcoll_v1.0.0.5
$portada = "$path\portada.htm"
$portada1 = "$path\portada1.htm"
$portada2 = "$path\portada2.htm"

$client = new-object System.Net.WebClient
$client.DownloadFile(“https://www.win.tue.nl/hashclash/fastcoll_v1.0.0.5.exe.zip”,“$path\fastcoll.zip”)
Expand-Archive -Path $path\fastcoll.zip -DestinationPath $path
C:\Users\Usuario\Desktop\fastcoll_v1.0.0.5.exe -p $portada -o $portada1 $portada2 #Se ejecuta el programa, -p es para indicar el archivo base y -o indica los archivos de salida
#IMPORTANTE: AL EJECUTAR EL PROGRAMA PONER EL PATH COMPLETO SIN HACER REFERENCIA A VARIABLES PARA QUE SE EJECUTE EL PROGRAMA

Get-FileHash -Path $portada1,$portada2  -Algorithm MD5