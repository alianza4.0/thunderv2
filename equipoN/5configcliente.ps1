﻿# Este script une la maquina virtual del cliente Windows 10 al subdominio thundera.thundercats.local

$nombreMaquinaCliente = "ClienteSnarff"
$machineNameThundera = "THUNDERAPC"

Invoke-Command -VMName $nombreMaquinaCliente -ScriptBlock {
    #=================================================================
    #======================ConfigInicialClienteFlanders=======================
    #Configurar una IP estática
    $ipaddress = "10.0.20.10"
    $dnsaddress = "10.0.20.1"
    $newComputerName = "W10THUN"
    $DomainName = "thundera.thundercats.local"
    #Configuraciones para entrar al dominio
    $userCore = "THUNDERA\Administrator"
    $pass4UserCore = "hola123.,"

    # Desactivar Firewall
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
    netsh advfirewall Set allprofiles State Off 

    # Creamos una excepciÃ³n al protocolo ICMP para probar conectividad
    netsh advfirewall firewall add rule name="Allow Ping" protocol=icmpv4 dir=in action=allow

    #############################################################################
    # Automatizado

    $passCore = ConvertTo-SecureString -String $pass4UserCore -AsPlainText -Force
    $CredentialCore = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userCore, $passCore

    #$hostname = hostname
    # Add-Computer -DomainName $DomainName -ComputerName $hostname -NewName $newComputerName -Credential $CredentialCore -Restart
    Add-Computer -DomainName $DomainName -Credential $CredentialCore -Restart

    #Al final del script se reiniciara la maquina
}

# Este script agrega el usuario Snarff al subdominio thundera.thundercats.local
Invoke-Command  -VMName $machineNameThundera -ScriptBlock {
    dsadd user "CN=Snarff, CN=Users, DC=thundera, DC=Thundercats, DC=local" `
    -samid sttl `
    -upn snarff@thundera.thundercats.local `
    -pwd "hola123.," `
    -fn snarff
}

Invoke-Command  -VMName $nombreMaquinaCliente -ScriptBlock {
#modificar registro para abrir los .ps1 con powershell y no con notepad
    Set-ItemProperty  HKLM:\SOFTWARE\Classes\microsoft.powershellscript.1\Shell\Open\Command -Name "(Default)" -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" 
#modificar registro para permitir ejecutar scripts del subdominio
    New-Item 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Ranges\Range1'
    New-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Ranges\Range1' -Name file -Value 1
    New-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Ranges\Range1' -Name :Range -Value 10.0.20.2
}