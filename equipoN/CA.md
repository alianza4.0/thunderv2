# CA ROOT Linux
## Configuracion :  

Dentro de la **CA ROOT** se configura el ambiente con el que vamos a trabajar, para esto tambien copiamos el **openssl.cnf** al directorio que creamos: 
>**$ mkdir root_ca**      
>**$ cd root_ca**     
>**$ cp /etc/ssl/openssl.cnf .**      
>**$ vi openssl.cnf**     

Dentro del archivo que **openssl.cnf** vamos a modificar algunos parametros para que el archivo funcione como queremos:
>Dentro del archivo buscamos la linea donde nos dice el directorio donde se va a guardar todo y lo cambiamos para que sea el directorio local:      
>[ CA_default ]     
>dir	&emsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;= . 	# Where everything is kept      
>

Despues modificamos unos campos dentro del mismo archivo para que podamos firmar cualquier peticion:
>\# For the CA policy       
>[ policy_match ]       
>countryName &emsp;&nbsp;&nbsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;= optional     
>stateOrProvinceName&emsp;&emsp;= optional      
>organizationName&emsp;&emsp;&emsp;&nbsp;&nbsp;= optional       
>organizationalUnitName&emsp;= optional     
>commonName&emsp;&nbsp;&emsp;&emsp;&emsp;&emsp;= optional       
>emailAddress&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;= optional     

Configuramos los valores por default que se usara para el certificado

>[ req_distinguished_name ]
>countryName = Country Name (2 letter code)  
>countryName_default = MX  
>countryName_min = 2  
>countryName_max = 2  
>
>stateOrProvinceName = State or Province Name (full name)  
>stateOrProvinceName_default = CDMX  
>
>localityName = Locality Name (eg, city)  
>localityName_default = CU  
>
>0.organizationName = Organization Name (eg, company)  
>0.organizationName_default = UNAM

>commonName_default = CA ROOT

Finalmente agregamos unas líneas para la extension **v3_ca**:
>[  v3_ca  ]    
>subjectKeyIdentifier&emsp;&emsp;= hash     
>authorityKeyIdentifier&emsp;= keyid:always,issuer      
>basicConstraints&emsp;&emsp;&emsp;&nbsp;=critical,CA:true        
>keyUsage&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;=critical,digitalSignature,cRLSign,keyCertSign       

Creamos los directorios y archivos que va a utilizar nuestra **CA**
>**$ mkdir certs**  
>**$ mkdir crl**    
>**$ mkdir newcerts**   
>**$ mkdir private**    
>**$ touch serial**     
>**$ echo 0100 > serial**   
>**$ touch index.txt**  
>**$ touch crlnumber**  
>**$ echo 0100 > crlnumber**    

Generamos numeros aleatorios
>**$ openssl rand -out ./private/.rand 1024**

Generamos la llave RSA con un password 
>**$ openssl genrsa -out ./private/cakey.pem -des3 -rand ./private/.rand 2048**     
	1024 semi-random bytes loaded   
	Generating RSA private key, 2048 bit long modulus   
	.............................+++    
	..+++   
	e is 65537 (0x10001)    
	Enter pass phrase for ./private/cakey.pem: **hola123.,**    
	Verifying - Enter pass phrase for ./private/cakey.pem: **hola123.,**    
	
Generamos nuestro certificado autofirmado y como ya establecimos los valores predeterminados anteriormente, solo se presiona enter en todos los caso
>**$ openssl req -x509 -new -key ./private/cakey.pem -out cacert.pem -config openssl.cnf -days [dias validos]**
	Enter pass phrase for ./private/cakey.pem:
	You are about to be asked to enter information that will be incorporated
	into your certificate request.
	What you are about to enter is what is called a Distinguished Name or a DN.
	There are quite a few fields but you can leave some blank
	For some fields there will be a default value,
	If you enter '.', the field will be left blank.
	-----
	Country Name (2 letter code) [MX]:  
	State or Province Name (full name) [CDMX]:  
	Locality Name (eg, city) [CU]:  
	Organization Name (eg, company) [UNAM]:  
	Organizational Unit Name (eg, section) []:  
	Common Name (e.g. server FQDN or YOUR name) []:  
	Email Address []:
	
## Firma de CA Subordinada

Para la firma de la **CA Subordinada** se requiere tener la peticion en el equipo y conocer su ubicacion exacta, despues de eso solo se ejecuta el siguiente comando:
>**$ openssl ca -in [cert].req -extensions v3_ca -config openssl.cnf -out [cert].crt**  
>**donde :**    
> &emsp;&emsp;&nbsp;**[cert]** : es el nombre del certificado   

Finalmente regresamos la peticion firmada junto con nuestro certificado autofirado
> - **[cert].crt**    
> - **cacert.pem**


