﻿# Este script crea la relacion de confianza entre los dominios thundercats.local y tercerplaneta.local

# Variables en las cuales se coloca el nombre de las maquinas virtuales
$machineNameThundercats = "THUNDERCATSPC"
$machineNameTercerPlaneta = "TERCERPLANETAPC"

# Se configura el conditional forwarder en TercerPlaneta
Invoke-Command  -VMName $machineNameTercerPlaneta -ScriptBlock {
    $destinationForest = "10.0.20.1"
    $strRemoteForest = "Thundercats.local"
    
    #=========================AñadirConditionalForwarder=====================
    #Añadimos el forwarding
    Add-DnsServerConditionalForwarderZone -Name $strRemoteForest `
    -ReplicationScope "Forest" -MasterServers $destinationForest
}

# Se configura el conditional forwarder en Thundercats
Invoke-Command  -VMName "THUNDERCATSPC" -ScriptBlock {
    # Variables
    $destinationForest = "10.0.21.1"
    $strRemoteForest = "TercerPlaneta.local"
    $strRemoteAdmin = "TERCERPLANETA\Administrator"
    $strRemoteAdminPassword = "hola123.,"

    #========================================================================
    #=========================AñadirConditionalForwarder=====================
    #Añadimos el forwarding
    Add-DnsServerConditionalForwarderZone -Name $strRemoteForest `
    -ReplicationScope "Forest" -MasterServers $destinationForest

    #=================================CreacionDelBidirectionalTrust=================================
    #Obtenemos los parametros del bosque remoto
    $remoteContext = New-Object -TypeName "System.DirectoryServices.ActiveDirectory.DirectoryContext" `
    -ArgumentList @( "Forest", $strRemoteForest, $strRemoteAdmin, $strRemoteAdminPassword)

    #Creamos un objeto de tipo bosque
    $remoteForest = [System.DirectoryServices.ActiveDirectory.Forest]::getForest($remoteContext)

    #obtenemos la config del bosque local
    $localforest=[System.DirectoryServices.ActiveDirectory.Forest]::getCurrentForest()

    #Creamos la relacion de confianza
    $localForest.CreateTrustRelationship($remoteForest,"Bidirectional")
    echo "CreateTrustRelationship: Succeeded for domain $($remoteForest)"

    #
    Get-ADTrust -Filter * -Credential TercerPlaneta.local
}