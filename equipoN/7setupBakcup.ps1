# Este script crea el backup del dominio thundercats.local

# Nombre de la maquina virtual del dominio thundercats.local
$machineNameThundercats = "THUNDERCATSPC"

Invoke-Command  -VMName $machineNameThundercats -ScriptBlock {
    Get-WindowsFeature -Name "Backup" 
    Install-WindowsFeature -Name Windows-Server-Backup
    #Se obtienen e instalan los features necesarios para hacer el backup.


    $jobName = "Backup8pmViernes" #Se asigna el nombre de la tarea programada.
    $triggerTask = New-JobTrigger -Weekly -At "8:00 PM" -DaysOfWeek "Friday" -WeeksInterval 1 
    #Se crea el trigger, indicando dia, hora, y numero de repeticiones del backup.


    #El codigo de abajo, crea la tarea programada, indicando el drive donde se guardara el backup
    Register-ScheduledJob -Name $jobName -Trigger $triggerTask -ScriptBlock {
        $BackupPolicy = New-WBPolicy
        $BackupPolicy | Add-WBSystemState
        $Volumes = Get-WBVolume -CriticalVolumes
        Add-WBBackupTarget -Policy $BackupPolicy -Volume $Volumes
        ####### NOTA: Cambiar el Drive donde se guardara el drive. ############
        $BackupDrive = New-WBBackupTarget -VolumePath 'C:\Users\Administrator\Desktop'
        Add-WBBackupTarget -Policy $BackupPolicy -Target $BackupDrive
        Start-WBBackup -Policy $BackupPolicy
    }
}