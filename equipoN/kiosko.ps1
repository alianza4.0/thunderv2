New-ADOrganizationalUnit -Name "Kiosko" -Path "DC=kiosko, DC=local" -ProtectedFromAccidentalDeletion $False
New-ADUser -SamAccountName "kiosko" -GivenName "Kiosko" -DisplayName "Kiosko" -Path "OU=Kiosko,DC=kiosko,DC=local" -Name "Kiosko"
dsmod user "CN=kiosko,OU=kiosko,DC=kiosko,DC=local" -pwd "hola123.," -disabled no
New-GPO -Name "Kiosko2" | New-GPLink -Target "OU=Kiosko, DC=kiosko, DC=local"
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoControlPanel -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoDesktop -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoTrayContextMenu -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Windows\Explorer" -ValueName ForceStartSize -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName LockTaskbar -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoChangeStartMenu -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoRun -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Windows\Explorer" -ValueName NoUninstallFromStart -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName TaskbarLockAll -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Windows\System" -ValueName DisableCMD -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName RestrictRun -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\RestrictRun" -Type String -Value '"C:\Program Files\Internet Explorer\iexplore.exe"'
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System" -ValueName Shell -Type String -Value '"C:\Program Files\Internet Explorer\iexplore.exe" -k'
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System" -ValueName DisableTaskMgr -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System" -ValueName DisableChangePassword -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoViewOnDrive -Type DWord -Value 67108863
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" -ValueName NoWinKeys -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoBrowserContextMenu -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoBrowserClose -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Control Panel" -ValueName HomePage -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Main" -ValueName "Start Page" -Type String -Value "kiosko.local"
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoFileNew -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoFileOpen -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoOpeninNewWnd -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoPrinting -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoBrowserOptions -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoFindFiles -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKLM\Software\Policies\Microsoft\Internet Explorer\Main" -ValueName DisableAddSiteMode -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoBrowserSaveAs -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Infodelivery\Restrictions" -ValueName NoBrowserSaveWebComplete -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoSelectDownloadDir -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKLM\Software\Policies\Microsoft\Internet Explorer\IEDevTools" -ValueName Disabled -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Restrictions" -ValueName NoViewSource -Type DWord -Value 1
Set-GPRegistryValue -Name "Kiosko2" -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\CaretBrowsing" -ValueName EnableOnStartup -Type DWord -Value 0
Set-GPRegistryValue -Name "Kiosko2" -Key "HKLM\Software\Policies\Microsoft\Internet Explorer\Toolbar" -ValueName Locked -Type DWord -Value 1