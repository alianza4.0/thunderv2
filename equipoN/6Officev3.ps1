﻿#nombre del equipo asignado al subdominio thundera.thundercats.local
$VMachine = "THUNDERAPC" 
#destino 
$pathDest = "C:\temp\o2013.zip" 
#ubicacion dentro de la maquina fisica del office
$pathOrig = "C:\Users\Administrator\Documents\o2013.zip" 

#IMPORTANTE, el office debe estar en formato .ZIP de otra forma no podrá realizarse lo siguiente
#Comando para copiar el zip de la maquina fisica al core.

Copy-VMFile -Name $VMachine -SourcePath $pathOrig -DestinationPath $pathDest -FileSource Host -CreateFullPath -Force
Invoke-Command  -VMName $VMachine -ScriptBlock {
    powershell
    cd C:\temp\
    #descomprimir el archivo dentro de la carpeta C:\temp
    Expand-Archive -Path .\o2013.zip -DestinationPath c:\temp
    #crear la carpeta compartida
    New-SmbShare -name "office" -path "C:\temp\office2013" -fullAccess "Everyone"    

    #Politicas internet snarff

    Set-GPPrefRegistryValue -Action Create -Context User -Key HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run -Name "Default Domain Policy" -Type String -Value iexplore -ValueName internet
    Set-GPPrefRegistryValue -Action create -Context User -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Control Panel" -Name "Default Domain Policy" -Type DWORD -Value 1 -ValueName Homepage
    Set-GPPrefRegistryValue -Action create -Context User -Key "HKCU\Software\Policies\Microsoft\Internet Explorer\Main" -Name "Default Domain Policy" -Type String -Value cubilfelino.thundercats.local -ValueName "Start Page"
    Set-GPPrefRegistryValue -Action create -Context User -Key "HKCU\Software\Microsoft\Internet Explorer\Main" -Name "Default Domain Policy" -Type String -Value cubilfelino.thundercats.local -ValueName "Start Page"

    New-GPLink -Name "Default Domain Policy" -Target "DC=thundera,DC=thundercats,DC=local"
    Set-GPPermission -Name "Office" -TargetName "Authenticated Users" -TargetType User -PermissionLevel GpoApply
    Set-GPPermission -Name "Office" -TargetName "Snarff" -TargetType User -PermissionLevel GpoApply
    gpupdate /force 


    #Politicas para la instalación de office
    New-GPO -Name "Office"

    Set-GPRegistryValue -Name "Office" -Key "HKCU\Software\Policies\Microsoft\Windows\PowerShell" -ValueName EnableScripts -Type DWord -Value 1
    Set-GPRegistryValue -Name "Office" -Key "HKCU\Software\Policies\Microsoft\Windows\PowerShell" -ValueName ExecutionPolicy -Type String -Value Unrestricted
    Set-GPRegistryValue -Name "Office" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System" -ValueName RunUserPSScriptsFirst -Type DWord -Value 1
    Set-GPRegistryValue -Name "Office" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run" -Type String -Value "\\10.0.20.2\office\Office.ps1"
    Set-GPRegistryValue -Name "Office" -Key "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\Run" -Type String -Value "\\10.0.20.2\office\Office.ps1"
    
    New-GPLink -Name "Office" -Target "DC=thundera,DC=thundercats,DC=local"
    Set-GPPermission -Name "Office" -TargetName "Authenticated Users" -TargetType User -PermissionLevel GpoApply
    Set-GPPermission -Name "Office" -TargetName "Snarff" -TargetType User -PermissionLevel GpoApply
    gpupdate /force 

    New-GPO -Name "NoOffice"

    Set-GPRegistryValue -Name "NoOffice" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun" -ValueName DisallowRun -Type String -Value "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Excel.Ink"
    Set-GPRegistryValue -Name "NoOffice" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun" -ValueName DisallowRun -Type String -Value "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Word.Ink"
    Set-GPRegistryValue -Name "NoOffice" -Key "HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowRun" -ValueName DisallowRun -Type String -Value "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Powerpoint.Ink"

    New-GPLink -Name "NoOffice" -Target "DC=thundera,DC=thundercats,DC=local"
    Set-GPPermission -Name "NoOffice" -TargetName "Authenticated Users" -TargetType User -PermissionLevel None
    Set-GPPermission -Name "NoOffice" -TargetName "Authenticated Users" -TargetType User -PermissionLevel GpoRead
    Set-GPPermission -Name "NoOffice" -TargetName "Administrator" -TargetType User -PermissionLevel GpoApply
    gpupdate /force 


}
