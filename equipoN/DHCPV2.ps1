$machineNameDHCP1 = "dhcpCore"
$machineNameDHCP2 = "dhcpFO"
$dominioDHCPcore = "DHCPCOREPC.dhcpCore.local"
$dominioDHCPFO = "DHCPFOPC.dhcpFO.local"


Invoke-Command  -VMName $machineNameDHCP2 -Credential DHCPFO\ADMINISTRATOR -ScriptBlock { #Credential puede o no ir 

#Instalar las caracteristicas de dhcp y dns (En este caso el servicio de DHCP esta en un controlador de dominio)
Install-WindowsFeature -Name DHCP -IncludeManagementTools
Install-WindowsFeature -Name DNS -IncludeManagementTools

#Anadir el dominio de dhcp2 en nuestro dc de dhcp1 
Add−DhcpServerInDC −DnsName $dominioDHCPFO −IPAddress 10.0.12.41
Add−DnsServerConditionalForwarderZone −Name $dominioDHCPcore −MasterServer 10.0.12.31
}


Invoke-Command  -VMName $machineNameDHCP1 -Credential DHCPCORE\ADMINISTRATOR -ScriptBlock { #Credential puede o no ir 

#Instalar las caracteristicas de dhcp y dns (En este caso el servicio de DHCP esta en un controlador de dominio)
Install-WindowsFeature -Name DHCP -IncludeManagementTools
Install-WindowsFeature -Name DNS -IncludeManagementTools

#Agregar el DHCP como DC
Add−DhcpServerInDC −DnsName $dominioDHCPcore  −IPAddress 10.0.12.31
#Agregar un scope
Add-DhcpServerv4Scope -name "coreScope" −StartRange 10.0.12.100 −EndRange 10.0.12.150 −SubnetMask 255.255.192.0 −State Active
Set-DhcpServerv4OptionValue −OptionID 3 −Value 10.0.0.1 −ScopeID 10.0.0.0 −ComputerName $dominioDHCPcore
#Indicar que el scope agregado otorgara IP a el dominio thundercats
Set-DhcpServerv4OptionValue −DnsDomain thundercats.local −DnsServer 10.0.12.1

Add−DnsServerConditionalForwarderZone -Name $dominioDHCPFO −MasterServer 10.0.12.41
#Configurar la alta disponibilidad con un balance del 50%
Add−DhcpSrverv4FailOver −Name Core-FailOver −PartnerServer DHCPFOPC.dhcpFO.local −ScopeId 10.0.0.0 −LoadBalancePercent 50

#Con los siguientes dos comandos se obtienen las IP otorgadas por cada DC
#Get−DhcpServerv4Lease −ComputerName $dominioDHCPcore
#Get−DhcpServerv4Lease −ComputerName DHCPFOPC.dhcpFO.local
}