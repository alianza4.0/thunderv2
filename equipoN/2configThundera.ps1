# Sccript para configurar el subdominio thundera.thundercats.local

# Variable donde se pone el nombre que tiene la maquina virtual
$nombreMaquinaVirtual = "THUNDERAPC"

Invoke-Command  -VMName $nombreMaquinaVirtual -ScriptBlock {
    #config
    $domainName = "Thundera.Thundercats.local"
    $domainNetbiosName = "THUNDERCATS"

    #===============================================================
    #=======================InstalarRoles===========================
    #Obtener los roles disponibles
    # Get-WindowsFeature

    #De la lista, instalamos el rol de Active Directory Domain Services y el de DNS
    Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools
    Install-WindowsFeature DNS -IncludeManagementTools

    #========================ConfigBosqueSubdmain============================
    #Script para configurar bosque Thundercats.local
    Import-Module ADDSDeployment
    install-Addsdomain `
    -domaintype "childDomain" `
    -parentdomainname "thundercats.local" `
    -newdomainname "thundera"  `
    -credential ( credential )  `
    -NoRebootOnCompletion:$false

    #Al iniciar la configuracion, preguntara por la password de recuperacion
}