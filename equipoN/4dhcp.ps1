# Este script configura los dhcp hacia el dominio thundercats.local

# Variables de nombres de maquinas virtuales
# DHCP core "DCO1"
$machinenNameDHCP1 = "DHCPCore"
# DHCP Desktop Experience "DCO2"
$machinenNameDHCP2 = "DHCPDE"

Invoke-Command  -VMName $machinenNameDHCP1 -ScriptBlock {
    powershell
    Install-WindowsFeature DHCP -IncludeManagementTools
    Install-WindowsFeature DNS -IncludeManagementTools

    netsh dhcp add securitygroups 
    Restart-service dhcpserver

    Add-DhcpServerv4Scope -name "Asignar" `
    -StartRange 10.0.30.10 `
    -EndRange 10.0.30.100 `
    -SubnetMask 255.255.192.0 `
    -State Active
    
    Set-DhcpServerv4Scope -ScopeID 10.0.20.1 -LeaseDuration 1.00:00:00

    Set-DhcpServerv4OptionValue `
    -ScopeID 10.0.20.1 `
    -DnsDomain thundercats.local `
    -DnsServer 10.0.20.1

    Add-DhcpServerInDC -DnsName thundercats.local -IPAddress 10.0.20.1
    Get-DhcpServerv4Scope
    Restart-service dhcpserver
}

Invoke-Command  -VMName $machinenNameDHCP2 -ScriptBlock {
    Install-WindowsFeature DHCP -IncludeManagementTools
    Install-WindowsFeature DNS -IncludeManagementTools

    netsh dhcp add securitygroups 
    Restart-service dhcpserver

    Add-DhcpServerv4Scope -name "AsignarDE" `
    -StartRange 10.0.30.101 `
    -EndRange 10.0.30.200 `
    -SubnetMask 255.255.192.0 `
    -State Active
    
    Set-DhcpServerv4Scope -ScopeID 10.0.20.1 -LeaseDuration 1.00:00:00

    Set-DhcpServerv4OptionValue `
    -ScopeID 10.0.20.1 `
    -DnsDomain thundercats.local `
    -DnsServer 10.0.20.1

    Add-DhcpServerInDC -DnsName thundercats.local -IPAddress 10.0.20.1
    Get-DhcpServerv4Scope
    Restart-service dhcpserver
}

Invoke-Command  -VMName $machinenNameDHCP1 -ScriptBlock {
    Add-DnsServerConditionalForwarderZone -Name "DC01.thundercats.local" `
    -MasterServer 10.0.20.1
}
Invoke-Command  -VMName $machinenNameDHCP2 -ScriptBlock {
    #DHCP de Alta Disponibilidad
    #Se hizo un segundo controlador de dominio y se instalo el DHCP, al controlador 
    #de dominio se le puso dc02.failover.dhcpy se configuro un conditional forwarder 
    #para que se pudiera conectar al core dhcp.
    Add-DnsServerConditionalForwarderZone -Name "DC02.thundercats.local" `
    -MasterServer 10.0.20.1
}